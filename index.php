<?php
session_start();
require "models/Database.php";
require 'models/UserModel.php';

if(isset($_SESSION['user'])){
    $user = unserialize($_SESSION['user']);
    if($user->getIsEmployee()){
        header("Location: ./pages/employeeView.php");
        exit();
    }
    else {
        header("Location: ./pages/userView.php");
        exit();
    }

}

$error = "";

if(isset($_POST['register'])) {
    header("Location: ./pages/register.php");
    exit();
}


if(isset($_POST['login'])) {



    $username = $_POST['username'];
    $password = $_POST['password'];

    $user = UserModel::getUserByNameAndPassword($username,$password);

    if ($user != null){
        if ($user->getIsEmployee()){
            $_SESSION['user'] = serialize($user);
            header("Location: ./pages/employeeView.php");
            exit();
        }
        else{
            $_SESSION['user'] = serialize($user);
            header("Location: ./pages/userView.php");
            exit();
        }

    }else $error = '<div class="alert  alert-danger">
        <h3 class="alert-heading">ERROR!</h3>
        <p class="mb-0">Die eingegeben Daten sind falsch.</p>
    </div>';
}
?>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
</head>
<body>
<div class="text-center">
    <img src="./images/logo.png"  width="50%" "style="align-content: center">
</div>
<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="">Online-Bank</a>
    </div>
</div>
<!--End Navbar -->
<div class="container-fluid>
    <div class="space70"></div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <!-- $_Server um die Userdaten direkt aus der Datenbank überprüfen zu können-->
            <form action="index.php" method="POST" >
                <fieldset>
                    <h2 class="text-center mt-5">Anmeldung</h2>
                    <div class="space40"></div>
                    <div class="form-group mt-5">
                        <label for="exampleInputEmail1">Username:</label>
                        <input class="form-control" placeholder="Max Mustermann" name="username" type="text">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Passwort:</label>
                        <input class="form-control" placeholder="*****" name="password" type="password">
                    </div>
                    <p></p>
                    <div>
                        <button name="login" type="submit" class="btn btn-primary">Anmelden</button>
                    </div>

                </fieldset>
            </form>
            <form action="pages/register.php" method="POST"><button name="register" type="submit" class="btn btn-primary">Registrieren</button></form>
            <?php echo $error; ?>
        </div>
    </div>

</div>
</body>

</html>