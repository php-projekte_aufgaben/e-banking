<?php

session_start();
require "../models/Database.php";
require "../models/UserModel.php";
require "../models/TransactionModel.php";
$userdata = $_SESSION['user'];
$user = unserialize($userdata);


if (isset($_POST['transaction'])) {
    if (UserModel::getByIban($_POST['iban']) != null) {
        $transaction = new TransactionModel();
        $transaction->setDestinationIban($_POST['iban']);
        $transaction->setPurpose($_POST['purpose']);
        $transaction->setPaymentReference($_POST['paymentReference']);
        $transaction->setAmount(str_replace(',', '.', $_POST['amount']));
        $transaction->setUserUserId($user->getUserId());

        $transaction->addTransaction();

        $user->setBalance($user->getBalance() - $transaction->getAmount());
        $_SESSION['user'] = serialize($user);
        Header("Location: ../pages/userView.php");
    } else {
        echo "<div class=\"alert  alert-danger\">
        <h3 class=\"alert-heading\">;(</h3>
        <p class=\"mb-0\">Dieser IBAN existiert nicht!</p>
        </div>";
    }
}
?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
</head>
<body>
<div class="text-center">
    <img src="../images/logo.png"  width="50%" "style="align-content: center">
</div>
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid">
        <a class="navbar-brand" href="">Online-Bank</a>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link btn-dark" href="../pages/userView.php">Zurück zum Menü</a>
                </li>
            </ul>
        </div>
        <button onclick="window.location='logout.php';" type="button" class="btn btn-danger">Abmelden</button>
    </div>
</div>

<div class="container">
    <div class="space70"></div>
    <div class="row">
        <div class="col-md-6 offset-md-3">

            <form action="transaction.php" method="post">
                <fieldset>
                    <h2 class="text-center mt-5">Neue Überweisung</h2>
                    <div class="space40"></div>
                    <div class="form-group mt-5">
                        <label>IBAN-Empfänger:</label>
                        <input class="form-control" placeholder="AT34 23223 22332" name="iban" type="text">
                    </div>
                    <div class="form-group">
                        <label>Verwendungszeck:</label>
                        <input class="form-control" name="purpose" type="text">
                    </div>
                    <div class="form-group">
                        <label>Zahlungsreferenz:</label>
                        <input class="form-control" name="paymentReference" type="text">
                    </div>
                    <div class="form-group">
                        <label>Betrag:</label>
                        <input class="form-control" placeholder="00,00" name="amount" type="number" step="0.01" min="0">
                    </div>
                    <p></p>
                    <div class="text-center">
                        <button onclick="display()" name="transaction" type="submit" class="btn btn-primary">Überweisen und Drucken</button>
                        <script>
                            function display() {
                                window.print();
                            }
                        </script>
                    </div>
                </fieldset>

            </form>
        </div>
    </div>
</div>
</body>
</html>