<?php

session_start();
require "../models/Database.php";
require "../models/UserModel.php";
require "../models/TransactionModel.php";

$userdata = $_SESSION['user'];
$user = unserialize($userdata);
$transactions=[];

if(isset($_POST['search'])){
    $text = $_POST['text'];
    $transactions = TransactionModel::searchText($text);
    $transactions = array_reverse($transactions);
}




?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
</head>
<body>
<div class="text-center">
    <img src="../images/logo.png"  width="50%" "style="align-content: center">
</div>
<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid text-center">
        <a class="navbar-brand" href="userView.php">Online-Bank</a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link btn-dark" href="transaction.php">Neue Überweisung</a>
            </li>

        </ul>
    </div>
    <button onclick="window.location='logout.php';" type="button" class="btn btn-danger">Abmelden</button>

</div>
<!--End Navbar -->
<ul class="list-group ">
    <li class="list-group-item text-center list-group-item-info ">
        <h1><p><b><?=$user->getUsername()?></b><br></h1>
        <h2>€ <?=$user->getBalance()?></h2><br>
        <h4>Kontonummer: <?= $user->getIban()?></h4>
    </li>
</ul>

<div class="navbar navbar-expand-lg navbar-dark bg-primary ">
    <div class="container-fluid text-center">
        <a class="navbar-brand" href="">Suche </a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="userViewTextSearch.php">Text</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="userViewDateSearch.php">Datum</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="userViewValueSearch.php">Betrag</a>
            </li>
        </ul>
    </div>
</div>

<form action="userViewTextSearch.php" method="post">
    <div class="col-sm-12 mt-5 ml-3">
        <label>
            Von:
            <input type="text" name="text" value="">
        </label>
        <button name="search" type="submit" class="btn-primary">Suchen</button>
    </div>

</form>
<?php
if(isset($_POST['search'])){
    if(empty($transactions)){
        echo "<div class=\"alert  alert-danger\">
      <h3 class=\"alert-heading\">;(</h3>
      <p class=\"mb-0\">Suche ergab keinen Treffer.</p>
      </div>";
    }
}?>


<?php
?>
<ul class="list-group">
    <?php
    foreach ($transactions as $val) {
        if($val->getUserUserId() == $user->getUserId()){
            ?>

            <li class="list-group-item list-group-item-danger">
                <p><h3> Ausgang: -€ <?=$val->getAmount()?></h3>
                Verwendungszweck: <?=$val->getPurpose()?><br>
                Zahlungsreferenz: <?=$val->getPaymentReference()?><br>
                Datum: <?=$val->getDate()?></p>
            </li>

            <?php
        }
        else {
            ?>

            <li class="list-group-item list-group-item-success">
                <p><h3>Eingang: +€ <?=$val->getAmount()?></h3>
                Verwendungszweck: <?=$val->getPurpose()?><br>
                Zahlungsreferenz: <?=$val->getPaymentReference()?><br>
                Datum: <?=$val->getDate()?></p>
            </li>
            <?php
        }
    }
    ?>
</ul>


</div>
</body>

</html>