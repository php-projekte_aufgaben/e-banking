<?php

session_start();
require "../models/Database.php";
require "../models/UserModel.php";
require "../models/TransactionModel.php";

$userdata = $_SESSION['user'];
$user = unserialize($userdata);
$transactions = TransactionModel::getAllTransactions($user->getUserId(), $user->getIban());
$transactions = array_reverse($transactions);

?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
</head>
<body>
<div class="text-center">
    <img src="../images/logo.png"  width="50%" "style="align-content: center">
</div>
<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid text-center">
        <a class="navbar-brand" href="employeeView.php">Online-Bank</a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link btn-dark" href="../pages/employeeView.php">Zurück zum Menü</a>
            </li>
            <div>
                <button onclick="display()" name="transaction" type="submit" class="btn border btn-primary ml-3">Drucken</button>
                <script>
                    function display() {
                        window.print();
                    }
                </script>
            </div>
        </ul>
    </div>
    <button onclick="window.location='logout.php';" type="button" class="btn btn-danger">Abmelden</button>

</div>
<!--End Navbar -->

<table class="table table-striped table-bordered mt-5">
    <thead>
    <tr>
        <th>Transaktion</th>
        <th>Empfänger IBAN</th>
        <th>Betrag</th>
        <th>Verwendungszweck</th>
        <th>Zahlungsreferenz</th>
        <th>Datum</th>
        <th>Sender ID</th>
    </tr>
    </thead>
    <tbody>
    <?php
    require_once "../models/TransactionModel.php";
    $credentials = TransactionModel::getAllTrans();
    foreach ($credentials as $c){
        echo '<tr>';
        echo '<td>' . $c->getTransactionId() . '</td>';
        echo '<td>' . $c->getDestinationIban() . '</td>';
        echo '<td>' . $c->getAmount() . '</td>';
        echo '<td>' . $c->getPurpose() . '</td>';
        echo '<td>' . $c->getPaymentReference() . '</td>';
        echo '<td>' . $c->getDate() . '</td>';
        echo '<td>' . $c->getUserUserId() . '</td>';
        echo '</tr>';
    }
    ?>
    </tbody>
</table>
</div>
</body>
</html>