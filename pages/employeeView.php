<?php
session_start();
require "../models/Database.php";
require "../models/UserModel.php";
require "../models/TransactionModel.php";

$userdata = $_SESSION['user'];
$user = unserialize($userdata);
$transactions = TransactionModel::getAllTransactions($user->getUserId(), $user->getIban());
$transactions = array_reverse($transactions);


?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
</head>
<body class="text-center">
<img src="../images/logo.png"  width="50%" "style="align-content: center">
<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid text-center">
        <a class="navbar-brand" href="employeeView.php">Online-Bank</a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link btn-dark" href="depositPayout.php">Einzahlung/Auszahlung</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-info ml-3" href="employeeViewTransactions.php">Transaktionen</a>
            </li>
        </ul>
    </div>
    <button onclick="window.location='logout.php';" type="button" class="btn btn-danger">Abmelden</button>

</div>
<!--End Navbar -->


<ul class="list-group ">
    <li class="list-group-item text-center list-group-item-info ">
        <h2> Willkommen</h2>
        <h2 class="mt-3">Mitarbeiter <b><?=$user->getUsername()?></b></h2><br>
        </p>
    </li>
</ul>
<div class="mt-5">
<?php
if($user->getUserId()==3){

?>
<div align="center">
    <img src="../images/Thomas.jpg" alt="Customer" width="300">
</div>

<?php
} elseif ($user->getUserId()==1){

?>
<div align="center">
    <img src="../images/Michael.jpg" alt="Customer" width="300">
</div>
<?php
} elseif($user->getUserId()==2) {


?>
<div align="center">
    <img src="../images/Manuel.jpg" alt="Customer" width="300">
</div>
<?php
}
?>
</div>
</div>
</body>
</html>