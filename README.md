# E-Banking Sprint 3

Kleinprojekt: E-Banking App 

# Inhaltsverzeichnis
- [E-Banking Sprint 3](#e-banking-sprint-3)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Problemstellung](#problemstellung)
- [Hauptszenarien](#hauptszenarien)
- [Funktionale Anforderungen](#funktionale-anforderungen)
- [Zusätzliche Anforderunge](#zusätzliche-anforderunge)
- [Liefervereinbarung](#liefervereinbarung)
- [Zielumgebung](#zielumgebung)
- [UML Diagramm](#uml-diagramm)
- [Aufbau der Ordnerstruktur](#aufbau-der-ordnerstruktur)
- [Implementierung](#implementierung)
  - [Neuesten & wichtigsten Codeteile](#neuesten--wichtigsten-codeteile)
    - [**TransactionModel**](#transactionmodel)
    - [**UserModel**](#usermodel)
- [Testprotokoll](#testprotokoll)
  - [Employee Login](#employee-login)
  - [Employee DepositPayout](#employee-depositpayout)
  - [Employee Transaction](#employee-transaction)
  - [User Login](#user-login)
  - [User View](#user-view)
  - [User Transaction](#user-transaction)
  - [User Textsearch](#user-textsearch)
  - [User Datesearch](#user-datesearch)
  - [User Amountsearch](#user-amountsearch)
  - [User Logout](#user-logout)
  - [User Register](#user-register)

# Problemstellung

Du bist Programmierer im ARZ Innsbruck. Für einen neu zu gewinnenden Kunden soll eine E-Banking-App programmiert werden. Ein erster voll funktionsfähiger Prototyp mit reduzierter Basisfunktionalität soll den Kunden überzeugen, die Programmierung und das Hosting der App im ARZ durchführen zu lassen. Du und ein weiterer Mitarbeiter aus der Abteilung werden mit der Erstellung des Prototyps beauftragt, für den vom Abteilungsleiter in Absprache mit dem potentiellen Kunden einige funktionale Anforderungen und ein Basis-Szenario definiert wurden.

# Hauptszenarien

Kunden registrieren sich über die App per mobilem Device oder PC bei der Bank und erhalten ein Konto. Kunden können über ihr Konto Überweisungen auf Konten innerhalb derselben Bank tätigen. Kunden können am Bankschalter über einen Bankangestellten Beträge ein- und auszahlen. Kunden können ihre Kontobewegungen und Kontostände detailliert abfragen bzw. durchsuchen.

# Funktionale Anforderungen

Folgende funktionale Anforderungen gelten für das Projekt:
1.	Kunden sollen sich selbst registrieren können.
2.	Kunden und alle anderen Benutzer des Systems sollen sich einloggen können.
3.	Nach einem Login sollen Benutzer solange eingeloggt bleiben, bis sie sich wieder abmelden oder aber den Browser schließen.
4.	Benutzer haben verschiedene Rollen: Es gibt zwei Rollen: „Kunde“, „Angestellter“.
5.	Angestellte werden vorab „händisch“ in die Datenbank eingetragen (es gibt also keinen Registrierungsprozess).
6.	Kunden haben genau 1 Konto, das sie gleich bei der Registrierung bekommen. Für dieses Bankkonto werden folgende Informationen gespeichert: Kontostand, alle Kontobewegungen (Einzahlungen, Auszahlungen), IBAN, BIC und Verfüger (=Kunde).
7.	Eingeloggte Kunden sollen ihre Kontodaten (Kontonummer, Kontostand, Eingänge und Ausgänge incl. allen Überweisungsdetails) abfragen können.
8.	Eingeloggte Kunden sollen Überweisungen (=Kontobewegung) auf andere Kontonummern der gleichen Bank durchführen können. Überweisungen an Kontonummern außerhalb der Bank werden später implementiert. Eine Überweisung hat folgende Informationen: IBAN und BIC Absender, IBAN und BIC Empfänger, Zahlungsreferenz, Verwendungszweck, Betrag in Euro auf 2 Kommastellen, Datum, Uhrzeit.
9.	Kunden sollen ihre Überweisungen nach Datum durchsuchen können (genau, von – bis).
10.	Kunden sollen ihre Überweisungen nach Beträgen (genau, von – bis) durchsuchen können.
11.	Kunden sollen ihre Überweisungen nach allen anderen Text-Informationen durchsuchen können.
12.	Angestellte (Kassa) können im Namen von Kunden Beträge auf das Konto einzahlen und abheben (Barbehebung).
13.	Kunden erhalten bei Einzahlung / Auszahlung über einen Bankbeamten (Kassa in der Bank) einen entsprechenden Beleg (ausgedruckt).

# Zusätzliche Anforderunge

1.	Responsive Bootstrap-UI
2.	Mehrschichtanwendung (UI, Modelklassen, ggf. zusätzliche Klassen für Businesslogik (Serviceklassen), DB-Zugriff mit Active-Record)
3.	Vollständige Sourcecode Dokumentation
4.	Codeversionierung mit Git und GitLab (Tools: https://tortoisegit.org/ , https://gitforwindows.org/)
5.	Scrum-Workmanagement mit GitLab (https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/, https://www.youtube.com/watch?v=CiolDtBIOA0)
6.	UI-Design in Adobe-XD (https://www.youtube.com/watch?v=m-vji_2H3W8&t=814s, https://www.youtube.com/watch?v=s4SXQb6e0uM&t=1564s)
7.	Arbeit im Pair-Programming-Modus

# Liefervereinbarung

Abzugeben sind:
1.	Projektplanung, Anforderungsanalyse und Systementwurf lt. Wordvorlage
2.	Quellcode der Anwendung als GitLab-Repository-URL
3.	SQL-Dump der Datenbank

# Zielumgebung

PHP-Mehrschichtanwendung incl. Bootstrap UI und MySQL-Datenbankanbindung.

# UML Diagramm

![UML-Diagramm](Diagramm/UML.png)

# Aufbau der Ordnerstruktur

**e-banking3**
+ **css**
+ **images**
+ **js**
+ **models**
  + Database.php (Class)
  + Databse.php (Interface)
  + TransactionModel (Class)
  + UserModel (Class)
+ **pages**
  + depositPayout.php
  + employeeView.php
  + employeeViewTransaction.php
  + logout.php
  + register.php
  + transaction.php
  + userView.php
  + userViewDateSearch.php
  + userViewTextSearch.php
  + userViewValueSearch.php
+ **sql**
  + sqldump.sql
+ index.php

# Implementierung

Implementierung des Source Codes

## Neuesten & wichtigsten Codeteile

**Anmerkung:**  <br>
Es gibt bereits eine ausarbeitung der CRUD operationen sowie eine Dokumentation vieler Codesegmente die sich in dieser erweiterung befinden. Aus diesem Grund wird nur vereinzelt bzw. auf bestimmte Codesegmente eingegangen. Weitere Erklärungen sowie Codedokumentationen sind auf GitLab vorzufinden.
<br>

### **TransactionModel**

Folgende Functionen in der TransactionModel Klasse sind wichtig bzw. wurden hinzugefügt.

Diese Function ist statisch, hat zwei Parametern (id, iban) und gibt alle Transaktionen des gewünschten Users aus. 
```php
   public static function getAllTransactions($id, $iban){

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction WHERE destinationIban = ? OR user_user_id = ?");
        $stmt->execute([$iban,$id]);
        $transArray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transArray;
    }
```
<br>


Diese Funktion gibt alle Transaktionen absteigend aus, ist statisch und hat keine Parameter.
```php
    public static function getAllTrans(){

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction ORDER BY date DESC");
        $stmt->execute();
        $transArray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transArray;
    }
```
<br>

Mit dieser Funktion werden Transaktionen hinzugefügt sowie die Akturalisierung des Kontostandes des Versenders & Senders.
```php
    public function addTransaction(){
        //erstelle Transaction
        $this->create();
        $pdo = Database::connect();

        //update Kontostand versender
        $user = UserModel::get($this->user_user_id);
        $user->setBalance($user->getBalance()-$this->amount);
        $user->update();

        //update empfänger
        $receivingUser = UserModel::getByIban($this->destinationIban);
        $receivingUser->setBalance($receivingUser->getBalance()+$this->amount);
        $receivingUser->update();

        Database::disconnect();

    }
```
<br>

Durch diese statische Funktion wird nach dem Verwendungszweck gesucht & sie besitzt einen Parameter (filter).
```php
    public static function searchText($filter){
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction WHERE purpose LIKE ? OR paymentReference LIKE ?");
        $stmt->execute([$filter,$filter]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;

    }
```
<br>

Deise Funktion ist ebenfalls statisch und bestizt zwei Parameter (firstDate, secondDate) um nach einem bestimmten Datum oder einem Zeitraum zu suchen. 
Datenfelder werden zuerst initialisert mit der jeweiligen Zeit die einen ganzen Tag ausmacht. Diese werden durch die execute Methode mitgegeben damit in dem entsprechenden Zeitraum von 00:00:00 - bis 23:59:59 gesucht werden kann.
```php 
    public static function searchDate($firstDate, $secondDate){
        $firstDateConv = $firstDate.' 00:00:00';
        $secondDateConv = $secondDate.' 23:59:59';

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction  WHERE date BETWEEN ? AND ?");
        $stmt->execute([$firstDateConv, $secondDateConv]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;
    }
```
<br>

Hier befindet sich erneut eine statische Funktion die zwei Parameter (firstValue, secondValue) besitzt. 
Die ersten beiden Datenfelder werden dafür verwendet um nachkommastellen darzustellen bzw. mit **Kommas** anzuzeigen, da diese normalerweise mit Punkten angezeigt werden.
```php
    public static function searchValue($firstValue, $secondValue){
        $firstValue = str_replace(",",".", $firstValue);
        $secondValue = str_replace(",",".", $secondValue);

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction  WHERE amount BETWEEN ? AND ?");
        $stmt->execute([$firstValue, $secondValue]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;
    }
```

<br>


Diese Funktion ist nicht statisch und hat einen Prameter (iban). sie wird verwendet um einen Transaktion zu erstellen sowie die Aktualisierung bzw. die Auszahlung/Darstellung am Kontostandes.
```php 
 public function payout($iban){
        //erstelle Transaction
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM user WHERE iban = ?");
        $stmt->execute([$iban]);
        $usertemp = $stmt->fetchObject("UserModel");
        $userid = $usertemp->getUserId();
        $this->createPayout($userid);

        //update Kontostand auszahlen
        $user = UserModel::get($userid);
        $user->setBalance($user->getBalance()-$this->amount);
        $user->update();

        Database::disconnect();
    }
```
<br>

Diese Funktion bestizt keinen Parameter und dient lediglich zum Erstellen einer Transaktion sowie die Aktualisierung bzw. die Einzahlung/Darstellung am Kontostandes. 
```php
    public function deposit(){
        //erstelle Transaction
        $this->create();
        $pdo = Database::connect();


        //update empfänger einzahlen
        $receivingUser = UserModel::getByIban($this->destinationIban);
        $receivingUser->setBalance($receivingUser->getBalance()+$this->amount);
        $receivingUser->update();

        Database::disconnect();
    }
```
<br>

### **UserModel**

In der UserModel Klasse werden einige der CRUD Methoden implementiert und nennenswert sind hier die fogenden zwei:


Diese Funktion ist statisch und besitzt zwei Parameter (username, password). Diese Funktion gibt den gewünschten Username und das dazugehörige Passwort aus, da wir diese Daten in der Login Funktion benötigt.

```php
   public static function getUserByNameAndPassword($username, $password){
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM user WHERE username = ? AND password = ?");
        $stmt->execute(array($username,$password));
        $data = $stmt->fetchObject('UserModel');
        Database::disconnect();
        return $data !== false ? $data : null;
    }
```

<br>

Die Funktion login hat keine Parameter und dient dazu das beim Login der Name und das Passwort überprüft wird mit der oben genannnten Funktion **getUserByNameAndPassword**.
```php 
    public function login(){
        $u = self::getUserByNameAndPassword($this->username, $this->password);
        if ($u != null){
            $_SESSION['email'] = $this->email;
            return true;
        }
        else{
            unset($_SESSION['email']);
            print_r("ES GIBT FALSE");
            return false;
        }

    }
```

# Testprotokoll

## Employee Login

![Login](Protokoll/Login.png)

![Employee](Protokoll/Employee.png)

![Employee_Login_Cookie1](Protokoll/Employee_Login_Cookie1.png)

![Employee-Header1](Protokoll/Employee_Header1.png)

![Employee-Header2](Protokoll/Employee_Header2.png)

## Employee DepositPayout

![Employee-DepositPayout](Protokoll/Employee_DepositPayout.png)

![Employee-DepositPayout-Cookie](Protokoll/Employee_DepositPayout_Cookie1.png)

![Employee-DepositPayout-Header1](Protokoll/Employee_DepositPayout_Header1.png)

![Employee-DepositPayout2](Protokoll/Employee_DepositPayout2.png)

![Employee-DepositPayout-Print](Protokoll/Employee_DepositPayout_Print.png)

![Employee-DepositePayout-Cookie2](Protokoll/Employee_DepositPayout_Cookie2.png)

![Employee-DepositePayout-Header2](Protokoll/Employee_DepositPayout_Header2.png)

## Employee Transaction

![Employee-Transaction](Protokoll/Employee_Transaction.png)

![Employee-Transaction-Cookie1](Protokoll/Employee_Transaction_Cookie1.png)

![Employee-Transaction-Header1](Protokoll/Employee_Transaction_Header1.png)

## User Login

![User-Login](Protokoll/User_Login.png)

![User-Login-Cookie1](Protokoll/User_Login_Cookie1.png)

![User-Login-Header1](Protokoll/User_Login_Header1.png)

![User-Login-Cookie2](Protokoll/User_Login_Cookie2.png)

![User-Login-Header2](Protokoll/User_Login_Header2.png)

## User View

![User-View](Protokoll/User_View.png)

![User-View-Cookie1](Protokoll/User_View_Cookie1.png)

![User-View-Header1](Protokoll/User_View_Header1.png)

![User-View-Cookie2](Protokoll/User_View_Cookie2.png)

![User-View-Header2](Protokoll/User_View_Header2.png)

## User Transaction

![User-Transaction](Protokoll/User_Transaction.png)

![User-Transaction-Cookie1](Protokoll/User_Transaction_Cookie1.png)

![User-Transaction-Header1](Protokoll/User_Transaction_Header1.png)

![User-Transaction-Print](Protokoll/User_Transaction_Print.png)

![User-Transaction-Cookie2](Protokoll/User_Transaction_Cookie2.png)

![User-Transaction-Header2](Protokoll/User_Transaction_Header2.png)

![User-Transaction-Cookie3](Protokoll/User_Transaction_Cookie3.png)

![User-Transaction-Header3](Protokoll/User_Transaction_Header3.png)

![User-Transaction2](Protokoll/User_Transaction2.png)

## User Textsearch

![User-Textsearch](Protokoll/User_Textsearch.png)

![User-Textsearch-Cookie1](Protokoll/User_Textsearch_Cookie1.png)

![User-Textsearch-Header1](Protokoll/User_Textsearch_Header1.png)

![User-Textsearch2](Protokoll/User_Textsearch2.png)

![User-Textsearch3](Protokoll/User_Textsearch3.png)

![User-Textsearch-Cookie2](Protokoll/User_Textsearch_Cookie2.png)

![User-Textsearch-Header2](Protokoll/User_Textsearch_Header2.png)

## User Datesearch

![User-Datesearch](Protokoll/User_Datesearch.png)

![User-Datesearch-Cookie1](Protokoll/User_Datesearch_Cookie1.png)

![User-Datesearch-Header1](Protokoll/User_Datesearch_Header1.png)

![User-Datesearch2](Protokoll/User_Datesearch2.png)

![User-Datesearch-Cookie2](Protokoll/User_Datesearch_Cookie2.png)

![User-Datesearch-Header2](Protokoll/User_Datesearch_Header2.png)

## User Amountsearch

![User-Amountsearch](Protokoll/User_Amountsearch.png)

![User-Amountsearch-Cookie1](Protokoll/User_Amountsearch_Cookie1.png)

![User-Amountsearch-Header1](Protokoll/User_Amountsearch_Header1.png)

![User-Amountsearch2](Protokoll/User_Amountsearch2.png)

![User-Amountsearch-Cookie2](Protokoll/User_Amountsearch_Cookie2.png)

![User-Amountsearch-Header2](Protokoll/User_Amountsearch_Header2.png)

## User Logout

![User-Logout](Protokoll/User_Logout.png)

![User-Logout-Cookie1](Protokoll/User_Logout_Cookie.png)

![User-Logout-Header1](Protokoll/User_Logout_Header.png)

![User-Logout-Cookie2](Protokoll/User_Logout_Cookie2.png)

![User-Logout-Header2](Protokoll/User_Logout_Header2.png)

## User Register

![User-Register](Protokoll/User_Register.png)

![User-Register-Cookie1](Protokoll/User_Register_Cookie1.png)

![User-Register-Header1](Protokoll/User_Register_Header1.png)

![User-Register2](Protokoll/User_Register2.png)

![User-Register-Cookie2](Protokoll/User_Register_Cookie2.png)

![User-Register-Header2](Protokoll/User_Register_Header2.png)

![User-Register-Cookie3](Protokoll/User_Register_Cookie3.png)

![User-Register-Header3](Protokoll/User_Register_Header3.png)
