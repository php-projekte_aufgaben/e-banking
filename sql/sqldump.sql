
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


-- -----------------------------------------------------
-- Schema bankdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bankdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bankdb` DEFAULT CHARACTER SET utf8 ;
USE `bankdb` ;

-- -----------------------------------------------------
-- Table `bankdb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bankdb`.`user` (
                                             `user_id` INT NOT NULL AUTO_INCREMENT,
                                             `username` VARCHAR(16) NOT NULL,
                                             `password` VARCHAR(32) NOT NULL,
                                             `iban` VARCHAR(255) NULL,
                                             `bic` VARCHAR(255) NULL,
                                             `balance` DECIMAL(10,2) NULL,
                                             `isEmployee` BOOLEAN NULL,
                                             PRIMARY KEY (`user_id`)); #ENGINE = InnoDB DEFAULT CHAR SET = utf8mb4;

INSERT INTO `bankdb`.`user` (`username`, `password`, `isEmployee`) VALUES ('Michael', '1234', true);
INSERT INTO `bankdb`.`user` (`username`, `password`, `isEmployee`) VALUES ('Manuel', '1234', true);
INSERT INTO `bankdb`.`user` (`username`, `password`, `isEmployee`) VALUES ('Thomas', '1234', true);


-- -----------------------------------------------------
-- Table `bankdb`.`transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bankdb`.`transaction` (
                                                    `transaction_id` INT NOT NULL AUTO_INCREMENT,
                                                    `destinationIban` VARCHAR(255) NOT NULL,
                                                    `destinationBic` VARCHAR(45) NULL,
                                                    `amount` DECIMAL(10,2) NOT NULL,
                                                    `purpose` VARCHAR(45) NULL,
                                                    `paymentReference` VARCHAR(45) NULL,
                                                    `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
                                                    `user_user_id` INT NOT NULL,
                                                    CONSTRAINT user_user_id
                                                        FOREIGN KEY (user_user_id)
                                                            REFERENCES user(user_id),
                                                    PRIMARY KEY (`transaction_id`)); #ENGINE = InnoDB DEFAULT CHAR SET = utf8mb4;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

