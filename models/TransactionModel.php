<?php
require_once 'Database.php';
require_once 'DatabaseObject.php';
require_once 'UserModel.php';


class TransactionModel implements DatabaseObject
{
    private $transaction_id;
    private $destinationIban;
    private $destinationBic;
    private $amount;
    private $purpose; // Verwendungszweck
    private $paymentReference; // Zahlungsreferenz
    private $date;
    private $user_user_id;

    private $errors = [];


    public function __construct()
    {

    }


    public static function getAllTransactions($id, $iban){

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction WHERE destinationIban = ? OR user_user_id = ?");
        $stmt->execute([$iban,$id]);
        $transArray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transArray;
    }

    public static function getAllTrans(){

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction ORDER BY date DESC");
        $stmt->execute();
        $transArray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transArray;
    }


    public function addTransaction(){
        //erstelle Transaction
        $this->create();
        $pdo = Database::connect();


        //update Kontostand versender
        $user = UserModel::get($this->user_user_id);
        $user->setBalance($user->getBalance()-$this->amount);
        $user->update();


        //update empfänger
        $receivingUser = UserModel::getByIban($this->destinationIban);
        $receivingUser->setBalance($receivingUser->getBalance()+$this->amount);
        $receivingUser->update();

        Database::disconnect();

    }

    public static function searchText($filter){
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction WHERE purpose LIKE ? OR paymentReference LIKE ?");
        $stmt->execute([$filter,$filter]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;

    }

    public static function searchDate($firstDate, $secondDate){
        $firstDateConv = $firstDate.' 00:00:00';
        $secondDateConv = $secondDate.' 23:59:59';

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction  WHERE date BETWEEN ? AND ?");
        $stmt->execute([$firstDateConv, $secondDateConv]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;
    }

    public static function searchValue($firstValue, $secondValue){
        $firstValue = str_replace(",",".", $firstValue);
        $secondValue = str_replace(",",".", $secondValue);

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction  WHERE amount BETWEEN ? AND ?");
        $stmt->execute([$firstValue, $secondValue]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;
    }

    public function payout($iban){
        //erstelle Transaction
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM user WHERE iban = ?");
        $stmt->execute([$iban]);
        $usertemp = $stmt->fetchObject("UserModel");
        $userid = $usertemp->getUserId();
        $this->createPayout($userid);

        //update Kontostand auszahlen
        $user = UserModel::get($userid);
        $user->setBalance($user->getBalance()-$this->amount);
        $user->update();

        Database::disconnect();
    }

    public function deposit(){
        //erstelle Transaction
        $this->create();
        $pdo = Database::connect();


        //update empfänger einzahlen
        $receivingUser = UserModel::getByIban($this->destinationIban);
        $receivingUser->setBalance($receivingUser->getBalance()+$this->amount);
        $receivingUser->update();

        Database::disconnect();
    }





    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @param mixed $transaction_id
     */
    public function setTransactionId($transaction_id)
    {
        $this->transaction_id = $transaction_id;
    }

    /**
     * @return mixed
     */
    public function getDestinationIban()
    {
        return $this->destinationIban;
    }

    /**
     * @param mixed $destinationIban
     */
    public function setDestinationIban($destinationIban)
    {
        $this->destinationIban = $destinationIban;
    }

    /**
     * @return mixed
     */
    public function getDestinationBic()
    {
        return $this->destinationBic;
    }

    /**
     * @param mixed $destinationBic
     */
    public function setDestinationBic($destinationBic)
    {
        $this->destinationBic = $destinationBic;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @param mixed $purpose
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;
    }

    /**
     * @return mixed
     */
    public function getPaymentReference()
    {
        return $this->paymentReference;
    }

    /**
     * @param mixed $paymentReference
     */
    public function setPaymentReference($paymentReference)
    {
        $this->paymentReference = $paymentReference;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getUserUserId()
    {
        return $this->user_user_id;
    }

    /**
     * @param mixed $user_user_id
     */
    public function setUserUserId($user_user_id)
    {
        $this->user_user_id = $user_user_id;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }


    public function create()
    {
        $pdo = Database::connect();
        $sql = "INSERT INTO transaction (destinationIban, amount, purpose, paymentReference, user_user_id) VALUES (?,?,?,?,?)";
        $stmt= $pdo->prepare($sql);
        $stmt->execute([$this->destinationIban, $this->amount, $this->purpose, $this->paymentReference, $this->user_user_id]);
        Database::disconnect();
    }

    public function createPayout($userid)
    {
        $pdo = Database::connect();
        $sql = "INSERT INTO transaction (destinationIban, amount, purpose, paymentReference, user_user_id) VALUES (?,?,?,?,?)";
        $stmt= $pdo->prepare($sql);
        $stmt->execute([$this->destinationIban, $this->amount, $this->purpose, $this->paymentReference, $userid]);
        Database::disconnect();
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public static function get($id)
    {
        // TODO: Implement get() method.
    }

    public static function getAll()
    {
        // TODO: Implement getAll() method.
    }
}
